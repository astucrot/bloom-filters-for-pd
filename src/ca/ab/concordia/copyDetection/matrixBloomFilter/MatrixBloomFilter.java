package ca.ab.concordia.copyDetection.matrixBloomFilter;

import java.util.ArrayList;
import java.util.List;
import java.util.BitSet;

public class MatrixBloomFilter {

    private static ArrayList<BitSet> matrix = new ArrayList<BitSet>();
    private static ArrayList<Paper> paperList = new ArrayList<Paper>();

    public void delAll() {
        MatrixBloomFilter.matrix.clear();
        MatrixBloomFilter.paperList.clear();
    }

    public void add(BitSet bitSet, Paper paper) {
        MatrixBloomFilter.matrix.add(bitSet);
        MatrixBloomFilter.paperList.add(paper);
    }

    public void add(BitSet bitSet, String authors, String publicationDate, String title) {
        MatrixBloomFilter.matrix.add(bitSet);
        Paper paper = new Paper();
        paper.setAuthors(authors);
        paper.setPublicationDate(publicationDate);
        paper.setTitle(title);
        MatrixBloomFilter.paperList.add(paper);
        paper = null;
    }

    public BitSet getBloomFilter(int index) {
        if (index < MatrixBloomFilter.matrix.size()) {
            return MatrixBloomFilter.matrix.get(index);
        } else {
            return null;
        }
    }

    public long getBloomFilterSize() {
        return MatrixBloomFilter.matrix.size();
    }

    public Paper getPaper(int index) {
        if (index < MatrixBloomFilter.paperList.size()) {
            return MatrixBloomFilter.paperList.get(index);
        } else {
            return null;
        }
    }

}
