/**
 * <p>Title: Paper</p>
 * <p>Description: </p>
 * <p>Company: Concordia University College of Alberta</p> 
 * @author    Lu Shi
 * @date       2013-03-19
 */
package ca.ab.concordia.copyDetection.matrixBloomFilter;

import java.util.HashMap;
import java.util.Map;

public class Paper
{

    private int ticketId;
    private String authors;
    private String title;
    private String publicationDate;
    private String status;
    private int elements_size = 0;
    BloomFilter bf;
    private Map<Integer, Double> similarPaperMap;

    public Paper(Paper paper)
    {
        ticketId = paper.getTicketId();
        String authors = paper.getAuthors();
        String title = paper.getTitle();
        String publicationDate = paper.getPublicationDate();
        String status = paper.getStatus();
        Map<Integer, Double> similarPaperMap = paper.getSimilarPaperMap();
    }

    public Paper()
    {
        ticketId = 0;
        String authors = "";
        String title = "";
        String publicationDate = "";
        String status = "";
        Map<Integer, Double> similarPaperMap = null;
    }

    public int getTicketId()
    {
        return ticketId;
    }

    public void setTicketId(int ticketId)
    {
        this.ticketId = ticketId;
    }

    public String getAuthors()
    {
        return authors;
    }

    public void setAuthors(String authors)
    {
        this.authors = authors;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getPublicationDate()
    {
        return publicationDate;
    }

    public void setPublicationDate(String publicationDate)
    {
        this.publicationDate = publicationDate;
    }

    /**
     * @return the similarPaperMap
     */
    public Map<Integer, Double> getSimilarPaperMap()
    {
        if (similarPaperMap == null) {
            similarPaperMap = new HashMap<Integer, Double>();
        }
        return similarPaperMap;
    }

    /**
     * @param similarPaperMap the similarPaperMap to set
     */
    public void setSimilarPaperMap(Map<Integer, Double> similarPaperMap)
    {
        this.similarPaperMap = similarPaperMap;
    }

    /**
     * @return the status
     */
    public String getStatus()
    {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status)
    {
        this.status = status;
    }

    public int getElements_size()
    {
        return elements_size;
    }

    public void setElements_size(int elements_size)
    {
        this.elements_size = elements_size;
    }

    public BloomFilter getBf()
    {
        return bf;
    }

    public void setBf(BloomFilter bf)
    {
        this.bf = bf;
    }
}
