/**
 * <p>
 * Title: FindingTask</p>
 * <p>
 * Description: Initializes the bloom filter and periodically checks the
 * database for entering a new task and then executes it</p>
 * <p>
 * Company: Altai State Technical Univesity</p>
 *
 * @author EvgeniyStorozhenko <evgeniy.storozhenko@gmail.com>
 * @date 2013-10-12
 */
package ca.ab.concordia.copyDetection.findingTask;

import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.logging.*;
import java.util.ArrayList;
import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;
import ca.ab.concordia.copyDetection.dao.model.TicketVO;
import ca.ab.concordia.copyDetection.dao.impl.TicketsDao;
import ca.ab.concordia.copyDetection.dao.impl.ITicketsDao;
import ca.ab.concordia.copyDetection.matrixBloomFilter.Paper;
import ca.ab.concordia.copyDetection.dao.model.ArticleSource;
import ca.ab.concordia.copyDetection.dao.impl.ArticlesSourcesDao;
import ca.ab.concordia.copyDetection.dao.impl.IArticlesSourcesDao;
import ca.ab.concordia.copyDetection.matrixBloomFilter.BloomFilter;
import ca.ab.concordia.copyDetection.service.MatrixBloomFilterService;
import ca.ab.concordia.copyDetection.util.configuration.ConfigurationManager;
import org.apache.log4j.BasicConfigurator;

public class FindingTask {

    private Map map;
    private int step;
    private Logger logger;
    private int sleep_time;
    private String log_path;
    private ITicketsDao dao;
    private int iterator = 1;
    private int streams_limit;
    private String output_XML;
    private Stopwatch stopwatch;
    private static FindingTask ft;
    public static int num_of_parts;
    private int max_length_document;
    private String input_users_files;
    public boolean load_all_sources;
    private boolean is_debug = false;
    private IArticlesSourcesDao daoAS;
    private int articles_sources_limit;
    public static int num_of_all_aricles;
    public int tickets_at_one_stream_limit;
    private ArrayList<Paper> paper_tickets;
    private ArrayList<Stream> streams_list;
    private MatrixBloomFilterService mbfService;
    private final int[] arr_loading = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    public FindingTask(String[] args) {
        checkDebug(args);
        getParametersFomConfig();
        initLogger();
        initModels();
        initContainers();
        initMatrixBloomFilter();
        findUnfinishedTickets();
    }

    public static void main(String[] args) {
        logConfigurate();
        ft = new FindingTask(args);
        ft.startFindigTasks();
    }

    /**
     * Check the input string parameters for the presence of debugging option
     *
     * @param args
     */
    private void checkDebug(String[] args) {
        for (String arg : args) {
            if (arg.equals("--debug")) {
                is_debug = true;
            }
        }
    }

    /**
     * Get the necessary parameters from the config
     */
    private void getParametersFomConfig() {
        log_path = ConfigurationManager.getValueByKey("Log_path");
        output_XML = ConfigurationManager.getValueByKey("OutputXML");
        step = Integer.parseInt(ConfigurationManager.getValueByKey("Step"));
        input_users_files = ConfigurationManager.getValueByKey("InputUsersFiles");
        sleep_time = Integer.parseInt(ConfigurationManager.getValueByKey("Sleep"));
        //streams_limit = Integer.parseInt(ConfigurationManager.getValueByKey("Streams_limit"));
        streams_limit = 1;
        max_length_document = Integer.parseInt(ConfigurationManager.getValueByKey("Max_length_document"));
        articles_sources_limit = Integer.parseInt(ConfigurationManager.getValueByKey("Articles_sources_limit"));
        tickets_at_one_stream_limit = Integer.parseInt(
                ConfigurationManager.getValueByKey("Tickets_at_one_stream_limit"));

        String load_all_src = ConfigurationManager.getValueByKey("OutputXML");
        if (load_all_src.equals("true")) {
            load_all_sources = true;
        } else {
            load_all_sources = false;
        }

        Stream.setFound_limit(Integer.parseInt(ConfigurationManager.getValueByKey("Found_limit")));
        Stream.setMin_procent(Double.parseDouble(ConfigurationManager.getValueByKey("Min_procent")));
    }

    /**
     * Creates objects to work with tickets and sources in the database
     */
    private void initModels() {
        dao = new TicketsDao();
        daoAS = new ArticlesSourcesDao();
        mbfService = new MatrixBloomFilterService();
    }

    /**
     * Creates containers
     */
    private void initContainers() {
        map = new HashMap();
        paper_tickets = new ArrayList<Paper>();
        streams_list = new ArrayList<Stream>();
    }

    /**
     * Create Logger
     */
    private void initLogger() {
        logger = Logger.getLogger(FindingTask.class.getName());
        try {
            FileHandler fh = new FileHandler(log_path);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
            logger.setUseParentHandlers(false);
            logger.addHandler(fh);

        } catch (SecurityException e) {
            logger.log(Level.SEVERE,
                    "Access denied. Not enough rights to create a file record.", e);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Input-ouput error.", e);
        }
        log("The program has been started.");
    }

    /**
     * Start searching tickets and execute those found
     */
    private void startFindigTasks() {
        for (;;) {
            if (streams_limit > streams_list.size()) {
                this.getTextFromFiles();
                this.reserveTicketsForThread();
                this.readTicketsFromDBToMatrix();
            }
            removeStreams();
            this.pause();
        }
    }

    /**
     * Get articles sources from DB and create bit arrays
     */
    private void initMatrixBloomFilter() {
        int now = 1;
        int amount = 0;
        String ids = "";
        int count = countArticlesWithLimits();
        System.out.println("Start initialization " + count + " articles in Bloom filter.");
        log("Start initialization " + count + " articles in Bloom filter.");
        stopwatch = new Stopwatch();
        try {
            do {
                String sql = "SELECT * FROM articles_sources ORDER BY id_article_source LIMIT " + now + "," + step + "";
                List<ArticleSource> list = daoAS.findBySql(sql, ArticleSource.class);
                int l_size = list.size();
                if (l_size == 0) {
                    break;
                }
                for (int i = 0; i < l_size; i++) {
                    updateProgress(amount / (double) count);
                    addArticleToBF(list.get(i));
                    amount++;
                    if (is_debug) {
                        ids += list.get(i).getId_article_source() + " ";
                    }
                }
                list.clear();
                now = amount + 1;
            } while (amount < count);

            log("Bloom filter initialization completed [" + stopwatch.stop() + " sec]");
            //log("ID initialized articles: " + ids);

            System.out.println("\nThe Copy Detection is ready.\n");
        } catch (SQLException ex) {
            Logger.getLogger(FindingTask.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Get articles sources from DB and create bit arrays for part of DB
     *
     * @param num
     */
    public void initPartOfMatrixBloomFilter(int num) {
        log("Bloom filter initialization " + (num + 1) + "/" + num_of_parts + " start");

        mbfService.getMatrixBloomFilter().delAll();
        int now = 1;
        now += num * articles_sources_limit;
        int amount = 0;
        String ids = "";
        stopwatch = new Stopwatch();
        try {
            do {
                String sql = "SELECT * FROM articles_sources ORDER BY id_article_source LIMIT " + now + "," + step + "";
                List<ArticleSource> list = daoAS.findBySql(sql, ArticleSource.class);
                int l_size = list.size();
                if (l_size == 0) {
                    break;
                }
                for (int i = 0; i < l_size; i++) {
                    addArticleToBF(list.get(i));
                    amount++;
                    if (is_debug) {
                        ids += list.get(i).getId_article_source() + " ";
                    }
                }
                list.clear();

                now = (num * articles_sources_limit) + amount + 1;
            } while (amount < articles_sources_limit); //|| ((num * articles_sources_limit+amount)<num_of_all_aricles)

        } catch (SQLException ex) {
            Logger.getLogger(FindingTask.class.getName()).log(Level.SEVERE, null, ex);
        }
        log("Bloom filter initialization " + (num + 1) + "/" + num_of_parts + " finished [" + stopwatch.stop() + " sec]");
    }

    /**
     * progress bar width in chars
     *
     * @param i
     * @param size
     */
    public void updateProgress(double progressPercentage) {
        final int width = 50; // progress bar width in chars

        System.out.print("\r[");
        int i = 0;
        for (; i <= (int) (progressPercentage * width); i++) {
            System.out.print(".");
        }
        for (; i < width; i++) {
            System.out.print(" ");
        }
        System.out.print("]");
    }

    public String getOutput_XML() {
        return output_XML;
    }

    /**
     * Sleep
     */
    private void pause() {
        try {
            TimeUnit.SECONDS.sleep(sleep_time);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * If the program was stopped at the time of processing tickets, they have
     * to be to check.
     */
    private void findUnfinishedTickets() {
        try {
            String s = "UPDATE tickets SET status='awaiting',stream_id=0 WHERE status=?";
            dao.saveBySql(s, "processed");
        } catch (SQLException ex) {
            Logger.getLogger(FindingTask.class.getName()).log(Level.SEVERE, null, ex);
        }
        TextExtractor.init();
    }

    /**
     * Get tasks from DB and create bit arrays
     */
    private void readTicketsFromDBToMatrix() {

        paper_tickets.clear();
        double falsePositiveProbability = Float.parseFloat(mbfService.getFalsePositiveProbability());
        int expectedNumberOfElements = Integer.parseInt(mbfService.getExpectedSize());
        try {

            String sql = "SELECT * FROM tickets WHERE status='processed' AND "
                    + "extracted_text IS NOT NULL AND stream_id='" + iterator + "'";
            if (tickets_at_one_stream_limit > 0) {
                sql += " LIMIT " + tickets_at_one_stream_limit;
            }

            List<TicketVO> list
                    = dao.findBySql(sql, TicketVO.class);

            if (list.size() > 0) {
                log("Found "
                        + list.size() + ((list.size() > 1) ? " tickets" : " ticket") + " for verification.");
                for (int i = 0; i < list.size(); i++) {

                    TicketVO vo = list.get(i);
                    if (vo != null) {
                        vo.clearText();

                        log("Creating BF for ticket id" + list.get(i).getTicket_id() + " start");
                        Stopwatch st_c = new Stopwatch();

                        BloomFilter bf = new BloomFilter(falsePositiveProbability, expectedNumberOfElements);
                        List<String> elements = mbfService.getElements(vo.getExtracted_text());
                        for (int j = 0; j < elements.size(); j++) {
                            bf.add(elements.get(j));
                        }
                        log("Creating BF for ticket id" + list.get(i).getTicket_id() + " finished [" + st_c.stop() + "s]");

                        Paper paper = new Paper();
                        paper.setTicketId(vo.getTicket_id());
                        paper.setStatus(vo.getStatus());
                        paper.setBf(bf);

                        log("Compearing bitsets and get similar map part 1/" + num_of_parts + " for ticket id" + list.get(i).getTicket_id() + " start");
                        Stopwatch st_sm = new Stopwatch();
                        paper.getSimilarPaperMap().putAll(mbfService.getSimilarRate(bf, elements.size()));
                        log("Compearing bitsets and get similar map part 1/" + num_of_parts + " for ticket id" + list.get(i).getTicket_id() + " finished  [" + st_sm.stop() + "s]");

                        paper_tickets.add(paper);
                    }

                }
                newStream();
            }
            list.clear();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Convert documents to text
     */
    private void getTextFromFiles() {
        try {
            List<TicketVO> list = dao.findBySql("SELECT ticket_id,extension FROM tickets "
                    + "WHERE status='awaiting' AND stream_id=0 AND extracted_text IS NULL", TicketVO.class);
            if (list.size() > 0) {
                for (TicketVO vo : list) {
                    stopwatch = new Stopwatch();
                    log("Start extracting text from " + vo.getExtension() + " for ticket " + vo.getTicket_id());

                    TextExtractor.init();
                    String text = "";
                    String path = input_users_files + "/" + vo.getTicket_id() + "." + vo.getExtension();
                    if (vo.getExtension().equals("pdf")) {
                        text = TextExtractor.getTextFromPDF(path);
                    } else if (vo.getExtension().equals("txt")) {
                        text = TextExtractor.getTextFromTXT(path);
                    } else if (vo.getExtension().equals("docx")) {
                        text = TextExtractor.getTextFromOfficeDocs(path);
                    } else if (vo.getExtension().equals("doc")) {
                        text = TextExtractor.getTextFromOfficeDocs(path);
                    } else if (vo.getExtension().equals("odt")) {
                        text = TextExtractor.getTextFromOfficeDocs(path);
                    } else if (vo.getExtension().equals("rtf")) {
                        text = TextExtractor.getTextFromOfficeDocs(path);
                    }

                    log("Extracting text from " + vo.getExtension() + " for ticket " + vo.getTicket_id() + " finished [" + stopwatch.stop() + " sec]");

                    String s = "UPDATE tickets SET extracted_text=? WHERE ticket_id=" + vo.getTicket_id() + "";
                    try {
                        dao.saveBySql(s, text);
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }

                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(FindingTask.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(FindingTask.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Reserve tickets for this thread
     */
    private void reserveTicketsForThread() {
        String s = "UPDATE tickets SET stream_id='" + iterator + "', status='processed',submission_time=NOW() "
                + "WHERE status = 'awaiting' AND stream_id=0";

        if (tickets_at_one_stream_limit > 0) {
            s += " LIMIT " + tickets_at_one_stream_limit;
        }

        try {
            dao.saveBySql(s);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Add bit array of article to Bloom filter
     *
     * @param article_src
     */
    private void addArticleToBF(ArticleSource article_src) {
        int start, end, num_strs, article_len, j, z;
        article_src.clearText();
        if (article_src.getText().length() > max_length_document) {
            num_strs = (int) Math.floor((article_src.getText().length()) / (double) max_length_document);
            String[] texts = new String[num_strs];
            for (j = 0; j < num_strs; j++) {
                start = j * max_length_document;
                end = (j + 1) * max_length_document;
                article_len = article_src.getText().length();
                if (end > article_len) {
                    end = article_len;
                }
                texts[j] = article_src.getText().substring(start, end).intern();
            }
            for (z = 0; z < num_strs; z++) {
                addStringToBF(article_src.getId_article_source(), texts[z]);
            }
        } else {
            addStringToBF(article_src.getId_article_source(), article_src.getText());
        }
    }

    /**
     * Add bit array of stirng to Bloom filter
     *
     * @param id_article_source
     * @param str
     */
    private void addStringToBF(int id_article_source, String str) {

        int expectedNumberOfElements = Integer.parseInt(mbfService.getExpectedSize());
        double falsePositiveProbability = Float.parseFloat(mbfService.getFalsePositiveProbability());

        BloomFilter bf = new BloomFilter(falsePositiveProbability, expectedNumberOfElements);
        List<String> elements = mbfService.getElements(str);
        for (int j = 0; j < elements.size(); j++) {
            bf.add(elements.get(j));
        }

        Paper paper = new Paper();
        paper.setTicketId(id_article_source);
        mbfService.getMatrixBloomFilter().add(bf.getBitSet(), paper);
    }

    /**
     * Count the number of articles based on taking into account the established
     * limit
     *
     * @return
     */
    private int countArticlesWithLimits() {
        int count = 0;
        try {
            count = daoAS.countBySql("SELECT COUNT(*) as num FROM articles_sources");
            num_of_all_aricles = count;
            num_of_parts = (int) Math.ceil(num_of_all_aricles / (double) articles_sources_limit);
            log("Num parts:" + num_of_parts);
        } catch (SQLException ex) {
            Logger.getLogger(FindingTask.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (articles_sources_limit != -1 && articles_sources_limit < count) {
            count = articles_sources_limit;
        }
        if (articles_sources_limit != -1 && articles_sources_limit < step) {
            step = articles_sources_limit;
        }

        return count;
    }

    /**
     * add/remove stream
     */
    public void newStream() {
        removeStreams();
        if (streams_limit > streams_list.size()) {
            streams_list.add(new Stream(iterator));
            streams_list.get(streams_list.size() - 1).setPaper_tickets((ArrayList<Paper>) paper_tickets.clone());
            streams_list.get(streams_list.size() - 1).setDaemon(true);
            streams_list.get(streams_list.size() - 1).start();
            iterator++;
        } else {
            log("Streams limit");
        }
        if (iterator > 1000) {
            iterator = 1;
        }
    }
    
    /**
     * Remove old streams
     */
    private void removeStreams() {
        int size = streams_list.size();
        for (int i = 0; i < size; i++) {
            if (!streams_list.get(i).isAlive()) {
                streams_list.remove(i);
                i--;
                size--;
            }
        }
    }

    /**
     * Log for pdfbox
     */
    private static void logConfigurate() {
        BasicConfigurator.configure();
        org.apache.log4j.Logger.getRootLogger().setLevel(org.apache.log4j.Level.OFF);
    }

    /**
     * Add message to log
     *
     * @param message
     */
    public void log(String message) {
        if (is_debug) {
            logger.info(message);
        }
    }

    public static FindingTask getFT() {
        return ft;
    }

    public ITicketsDao getDao() {
        return dao;
    }

    public IArticlesSourcesDao getDaoAS() {
        return daoAS;
    }

    public MatrixBloomFilterService getMbfService() {
        return mbfService;
    }

}
