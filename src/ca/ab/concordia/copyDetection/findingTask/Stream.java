/**
 * <p>
 * Title: Stream</p>
 * <p>
 * Description: Separate thread for quick and detailed search</p>
 * <p>
 * Company: Altai State Technical Univesity</p>
 *
 * @author EvgeniyStorozhenko <evgeniy.storozhenko@gmail.com>
 * @date 2013-10-12
 */
package ca.ab.concordia.copyDetection.findingTask;

import java.util.Map;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;
import java.sql.SQLException;
import ca.ab.concordia.copyDetection.dao.model.TicketVO;
import ca.ab.concordia.copyDetection.dao.model.ArticleSource;
import ca.ab.concordia.copyDetection.matrixBloomFilter.Paper;

public class Stream extends Thread {

    private int num;
    private int stream_id;
    private static int found_limit;
    private static double min_procent;
    private ArrayList<Stopwatch> stopwatch;
    private ArrayList<Paper> paper_tickets;
    private ArrayList<Plagiat> plagiat_list;

    public void run() {
        log("Found " + paper_tickets.size() + " tickets for verification searching");
        stopwatch = new ArrayList<Stopwatch>();
        plagiat_list = new ArrayList<Plagiat>();
        quickSearch();
        detailedSearch();
    }

    public Stream(int id) {
        stream_id = id;
    }

    /**
     * Get sources, of which perhaps copied text
     */
    private void quickSearch() {

        int size = paper_tickets.size();
        if (paper_tickets.size() > 0) {

            ArrayList<String> in = new ArrayList<String>();
            ArrayList<Float> coef = new ArrayList<Float>();

            log("Quick searching began");
            Stopwatch st1 = new Stopwatch();
            log("Compearing part 1 of " + size + " start");

            for (int i = 0; i < size; i++) {
                Stopwatch st1_i = new Stopwatch();
                log("Compearing coefficients part 1/" + FindingTask.num_of_parts + " id" + paper_tickets.get(i).getTicketId() + " start");
                in.add("0");
                coef.add(culcCoefficient(i));
                in.set(i, in.get(i) + findAppropriateIds(i, coef.get(i)));
                log("Compearing coefficients part 1/" + FindingTask.num_of_parts + " id" + paper_tickets.get(i).getTicketId() + " finished [" + st1_i.stop() + "s]");
            }
            log("Compearing part 1 of " + FindingTask.num_of_parts + " finished [" + st1.stop() + "s]");

            
            if (FindingTask.getFT().load_all_sources) {
                for (int j = 1; j < FindingTask.num_of_parts; j++) {

                    FindingTask.getFT().initPartOfMatrixBloomFilter(j);
                    Stopwatch st1_j = new Stopwatch();
                    log("Compearing part " + (1 + j) + " of " + FindingTask.num_of_parts + " start");
                    for (int i = 0; i < size; i++) {
                        log("Compearing bitsets and get similar map part " + (1 + j) + "/" + FindingTask.num_of_parts + " for ticket id" + paper_tickets.get(i).getTicketId() + " start");
                        Stopwatch st_sm = new Stopwatch();
                        paper_tickets.get(i).setSimilarPaperMap(null);
                        paper_tickets.get(i).getSimilarPaperMap().putAll(
                                FindingTask.getFT().getMbfService().getSimilarRate(
                                        paper_tickets.get(i).getBf(), paper_tickets.get(i).getElements_size()));
                        log("Compearing bitsets and get similar map part " + (1 + j) + "/" + FindingTask.num_of_parts + " for ticket id" + paper_tickets.get(i).getTicketId() + " finished  [" + st_sm.stop() + "s]");

                        Stopwatch st1_i = new Stopwatch();
                        log("Compearing coefficients part " + (1 + j) + "/" + FindingTask.num_of_parts + " id" + paper_tickets.get(i).getTicketId() + " start");
                        coef.set(i, culcCoefficient(i));
                        in.set(i, in.get(i) + findAppropriateIds(i, coef.get(i)));
                        log("Compearing coefficients part " + (1 + j) + "/" + FindingTask.num_of_parts + " id" + paper_tickets.get(i).getTicketId() + " finished [" + st1_i.stop() + "s]");
                    }
                    log("Compearing part " + (1 + j) + " of " + FindingTask.num_of_parts + " finished [" + st1_j.stop() + "s]");
                }
            }

            log("Quick searching end");

            for (int i = 0; i < size; i++) {
                try {
                    String select_tickets_sql = "SELECT * FROM tickets WHERE ticket_id=" + paper_tickets.get(i).getTicketId();
                    List<TicketVO> ticket = FindingTask.getFT().getDao().findBySql(select_tickets_sql, TicketVO.class);

                    if (!in.get(i).equals("0")) {
                        String found_limit_str = "";
                        if (found_limit != -1) {
                            found_limit_str = "LIMIT " + found_limit;
                        }
                        String select_sources_sql = "SELECT * FROM articles_sources WHERE id_article_source IN (" + in.get(i) + ") " + found_limit_str;
                        List<ArticleSource> list = FindingTask.getFT().getDaoAS().findBySql(select_sources_sql, ArticleSource.class);

                        plagiat_list.add(new Plagiat(paper_tickets.get(i).getTicketId(), ticket.get(0).getExtracted_text(), list));
                        log("List of sources id from quick search for ticket id" + paper_tickets.get(i).getTicketId() + ": " + in.get(i));
                    } else {
                        String s = "UPDATE tickets SET status='done',result_perc=0, checkout_time=NOW() WHERE ticket_id=?";
                        FindingTask.getFT().getDao().saveBySql(s, paper_tickets.get(i).getTicketId());
                        log("List of sources id from quick search for ticket id" + paper_tickets.get(i).getTicketId() + " is empty.");

                        ArrayList<ArticleSource> articles_sources = new ArrayList<ArticleSource>();
                        Comparator comparator = new Comparator(ticket.get(0).getExtracted_text(), articles_sources, paper_tickets.get(i).getTicketId());
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
            if (FindingTask.getFT().load_all_sources) {
                FindingTask.getFT().initPartOfMatrixBloomFilter(0);
            }
        }
        stopwatch.clear();
    }

    /**
     * Calculate the coefficient, from which, articles start allowed in a
     * detailed search
     *
     * @param i
     * @return
     */
    private float culcCoefficient(int i) {
        float summ = 0;
        int num1 = 0;
        int max_lim = 5;
        float[] max = new float[max_lim];
        for (int z = 0; z < max_lim; z++) {
            max[z] = 0;
        }

        float size_sim = ((float) paper_tickets.get(i).getSimilarPaperMap().size());
        Iterator it = paper_tickets.get(i).getSimilarPaperMap().entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry) it.next();
            float val = Float.parseFloat(pairs.getValue() + "");
            for (int z = 0; z < max_lim; z++) {
                if (val > max[z]) {
                    max[z] = val;
                    break;
                }
            }
            num1++;
            summ += val;
        }
        return (max[0] - (max[0] - max[(max_lim - 1)]));
    }

    /**
     * Returns a string identifier for sql query from sources, the level of
     * compliance bitmaps which satisfies the conditions
     *
     * @param i
     * @param coef
     * @return
     */
    private String findAppropriateIds(int i, float coef) {
        Iterator it = this.paper_tickets.get(i).getSimilarPaperMap().entrySet().iterator();
        String in = "";
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry) it.next();
            float val = Float.parseFloat(pairs.getValue() + "");
            if (coef <= val && val > min_procent) {
                in += "," + pairs.getKey();
            }
        }
        return in;
    }

    /**
     * Search parts of the text, which are suspected of plagiarism
     */
    private void detailedSearch() {
        if (plagiat_list.size() > 0) {
            int i = 0;
            for (Plagiat plag : plagiat_list) {
                stopwatch.add(i, new Stopwatch());
                log("Detailed searching began for the ticket with id " + plag.getTicket_id() + ".");
                Comparator cprt = new Comparator(plag.getText(), plag.getArticles_sources(), plag.getTicket_id());
                String s = "UPDATE tickets SET status='done',result_perc="
                        + (cprt.getResult_per() > 100 ? 100 : cprt.getResult_per())
                        + ",checkout_time=NOW() WHERE ticket_id=?";
                try {
                    FindingTask.getFT().getDao().saveBySql(s, plag.getTicket_id());
                } catch (SQLException ex) {
                    //Logger.getLogger(Stream.class.getName()).log(Level.SEVERE, null, ex);
                }

                log("Detailed searching ended for the ticket with id " + plag.getTicket_id() + ". Result: "
                        + (cprt.getResult_per() > 100 ? 100 : cprt.getResult_per()) + "% matches [" + stopwatch.get(i).stop() + " sec].");
                i++;
            }
            plagiat_list.clear();
        }
    }

    /**
     * Add message to log
     *
     * @param message
     */
    public void log(String message) {
        FindingTask.getFT().log(message);
    }

    public ArrayList<Paper> getPaper_tickets() {
        return paper_tickets;
    }

    public ArrayList<Plagiat> getPlagiat_list() {
        return plagiat_list;
    }

    public void setPaper_tickets(ArrayList<Paper> paper_tickets) {
        this.paper_tickets = null;
        this.paper_tickets = paper_tickets;
    }

    public void setPlagiat_list(ArrayList<Plagiat> plagiat_list) {
        this.plagiat_list = plagiat_list;
    }

    public void setStream_id(int stream_id) {
        this.stream_id = stream_id;
    }

    public int getStream_id() {
        return stream_id;
    }

    public static void setMin_procent(double min_procent) {
        Stream.min_procent = min_procent;
    }

    public static void setFound_limit(int found_limit) {
        Stream.found_limit = found_limit;
    }

    public static double getMin_procent() {
        return min_procent;
    }

    public static int getFound_limit() {
        return found_limit;
    }
}
