/**
 * <p>Title: Sentence</p>
 * <p>Description: part of text</p> 
 * @author EvgeniyStorozhenko <evgeniy.storozhenko@gmail.com>
 * @date 2013-10-20
 */
package ca.ab.concordia.copyDetection.findingTask;

public class Sentence {

        private int length = 0;
        private int start = 0;
        private int end = 0;
        private boolean found = false;
        private boolean drop = false;
        private String text = "";

        public int getLength() {
                return length;
        }

        public int getStart() {
                return start;
        }

        public int getEnd() {
                return end;
        }

        public boolean isFound() {
                return found;
        }

        public String getText() {
                return text;
        }

        public void setLength(int length) {
                this.length = length;
        }

        public void setStart(int start) {
                this.start = start;
        }

        public void setEnd(int end) {
                this.end = end;
        }

        public void setFound(boolean found) {
                this.found = found;
        }

        public void setText(String text) {
                this.text = text;
        }

	public boolean isDrop() {
		return drop;
	}

	public void setDrop(boolean drop) {
		this.drop = drop;
	}
	
	Sentence(int start, int end, String text) {
                this.start = start;
                this.end = end;
                this.text = text;
                this.length = text.length();
        }
}
