/**
 * <p>Title: Comparator</p>
 * <p>Description: Comparing suspicious text and list of sources</p>
 *
 * @author EvgeniyStorozhenko <evgeniy.storozhenko@gmail.com>
 * @date 2013-10-17
 */
package ca.ab.concordia.copyDetection.findingTask;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Random;
import org.w3c.dom.Element;
import java.util.ArrayList;
import java.io.IOException;
import java.io.StringWriter;
import org.w3c.dom.Document;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import javax.xml.transform.Transformer;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.TransformerException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import ca.ab.concordia.copyDetection.dao.model.ArticleSource;
import ca.ab.concordia.copyDetection.util.configuration.ConfigurationManager;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class Comparator {
    
    private String str1;
    public Document doc;
    public Element root;
    private double ave_p1;
    private int last = -1;
    private int result_per;
    private String[] words;
    private final int k = 5;
    private final int w = 4;
    private final int t = 3;
    private final Random rand;
    private int[] fingerprint;
    private final int delay = 3;
    private final int ticket_id;
    private int num_founded = 0;
    private int id_article_source;
    private final Double accuracy;
    private int plagiat_length = 0;
    public DocumentBuilder docBuilder;
    public DocumentBuilderFactory dbfac;
    private final ArrayList<Sentence> p1;
    private final ArrayList<Sentence> p2;
    private final ArticleSource suspicious;
    private final List<ArticleSource> articles_sources;

    Comparator(String text1, List<ArticleSource> articles_sources, int ticket_id) {

        accuracy = Double.parseDouble(ConfigurationManager.getValueByKey("Levenstein_distance_accuracy"));
        suspicious = new ArticleSource();
        suspicious.setText(text1.toLowerCase());
        Stopwatch sw = new Stopwatch();
        suspicious.createFingerprint(text1);
        log("Fingerprint for suspicious text "+suspicious.getId_article_source()+" created ["+sw.stop()+"].");
        suspicious.setFingerprint("");

        this.articles_sources = articles_sources;
        this.ticket_id = ticket_id;
        suspicious.setId_article_source(ticket_id);
        p1 = new ArrayList<Sentence>();
        p2 = new ArrayList<Sentence>();
        rand = new Random();
        dbfac = DocumentBuilderFactory.newInstance();
        
        try {
            docBuilder = dbfac.newDocumentBuilder();
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(Comparator.class.getName()).log(Level.SEVERE, null, ex);
        }
        doc = docBuilder.newDocument();
        findSimilar();
    }

    /**
     * Highlight part of text in LEFT editor from start to end
     *
     * @param start
     * @param end
     */
    public void markLeft(int start, int end) {
        plagiat_length += (end - start);
        // MainWindow.getDocLeft().setCharacterAttributes(start, (end - start), MainWindow.getColorBG((num_founded%19)) , false);
    }

    /**
     * Highlight part of text in RIGHT editor from start to end
     *
     * @param start
     * @param end
     */
    public void markRight(int start, int end) {
        // MainWindow.getDocRight().setCharacterAttributes(start, (end - start), MainWindow.getColorBG((num_founded%19)), false);
        num_founded++;
    }

    /**
     * Compearing text1 and text2
     */
    public void findSimilar() {

        int p1_num, p2_num, soureses_iterator = 0;
        boolean first;
        addToXmlRoot();
        splitSuspicious();
        p1_num = p1.size();

        for (ArticleSource source : articles_sources) { // Compare articles sources with suspicious
            id_article_source = source.getId_article_source();
            source.createFingerprintArrayFromString();

            if (compareFingerprints(source)) {
                Stopwatch sw = new Stopwatch();
                first = true;
                splitSource(soureses_iterator);
                p2_num = p2.size();

                for (int i = 0; i < p1_num; i++) { // Compare sentenses of suspicious
                    for (int j = 0; j < p2_num; j++) { // Compare sentenses of sources
                        if (kGrams(i, j)) {
                            if (similarity(i, j) >= accuracy) {
                                if (first) {
                                    addToXmlSource(source);
                                    first = false;
                                }
                                markSentensAsPlagiat(soureses_iterator, i, j);
                            }
                        } 
                    } // Compare sentenses of sources is over
                } // Compare sentenses of suspicious is over

                last = -1;
                p2.clear();
                log("Search using Levenshtein distance suspicious article "+suspicious.getId_article_source()
                        +" in the source article "+id_article_source+" is finished ["+sw.stop()+" sec].");
            }
            soureses_iterator++;
        } // Compare articles sources with suspicious is over

        saveDocumentToXML();
        suspicious.setText(suspicious.getText().replaceAll("\\r\\n", " ").replaceAll("\\pP|\\p{Punct}", ""));
        result_per = (int) (((double) plagiat_length / (double) suspicious.getText().length()) * 100);

    }

    /**
     * Split suspicious text to parts
     */
    private void splitSuspicious() {
        int cursor = 0;
        String tmp_str;
        String delimiter = "[\\.\\!\\?]";
        String[] temp = suspicious.getText().split(delimiter);

        for (int i = 0; i < temp.length; i++) {
            tmp_str = (new String(temp[i])).replaceAll("[\\.\\,\\!\\?\\:\\-]", "").trim().intern();
            if (!tmp_str.equals("") && tmp_str.length() > 3) {
                p1.add(new Sentence(cursor, (cursor + temp[i].length()), tmp_str));
            }
            cursor += temp[i].length();
            tmp_str = null;
        }
    }

    /**
     * Split source text to parts
     *
     * @param soureses_iterator
     */
    private void splitSource(int soureses_iterator) {
        int cursor = 0;
        String tmp_str = articles_sources.get(soureses_iterator).getText().toLowerCase().intern();
        String[] temp = tmp_str.split("[\\.\\!\\?]");

        for (int i = 0; i < temp.length; i++) {
            tmp_str = (new String(temp[i])).replaceAll("[\\.\\,\\!\\?\\:\\-]", "").trim().intern();
            if (!tmp_str.equals("") && tmp_str.length() > 3) {
                p2.add(new Sentence(cursor, (cursor + temp[i].length()), tmp_str));
            }
            cursor += temp[i].length();
            tmp_str = null;
        }
        p2.trimToSize();
    }

    /**
     * Mark part of text as plagiat
     *
     * @param soureses_iterator
     * @param i
     * @param j
     */
    private void markSentensAsPlagiat(int soureses_iterator, int i, int j) {
        p1.get(i).setFound(true);
        p2.get(j).setFound(true);
        markLeft(p1.get(i).getStart(), p1.get(i).getEnd());
        markRight(p2.get(j).getStart(), p2.get(j).getEnd());
        addToXmlInterval(
                articles_sources.get(soureses_iterator).getId_article_source(),
                p1.get(i).getStart(), p1.get(i).getLength(),
                p2.get(j).getStart(), p2.get(j).getLength());
    }

    /**
     * Levenshtein distance between string1 and string2
     *
     * @param S1 string1
     * @param S2 string2
     * @return
     */
    int levDist(String S1, String S2) {
        int m = S1.length(), n = S2.length();
        int[] D1 = null;
        int[] D2 = new int[n + 1];

        for (int i = 0; i <= n; i++) {
            D2[i] = i;
        }

        for (int i = 1; i <= m; i++) {
            D1 = D2;
            D2 = new int[n + 1];
            for (int j = 0; j <= n; j++) {
                if (j == 0) {
                    D2[j] = i;
                } else {
                    int cost = (S1.charAt(i - 1) != S2.charAt(j - 1)) ? 1 : 0;
                    if (D2[j - 1] < D1[j] && D2[j - 1] < D1[j - 1] + cost) {
                        D2[j] = D2[j - 1] + 1;
                    } else if (D1[j] < D1[j - 1] + cost) {
                        D2[j] = D1[j] + 1;
                    } else {
                        D2[j] = D1[j - 1] + cost;
                    }
                }
            }
        }
        return D2[n];
    }

    /**
     * Creat root
     */
    private void addToXmlRoot() {
        root = doc.createElement("pdrml");
        doc.appendChild(root);
        Element suspicious_document = doc.createElement("suspicious_document");
        Element title = doc.createElement("title");
        title.setTextContent("None");
        suspicious_document.appendChild(title);
        Element text = doc.createElement("text");
        text.setTextContent(suspicious.getText());
        suspicious_document.appendChild(text);
        root.appendChild(suspicious_document);
    }

    /**
     * Add source to xml as source of copy
     *
     * @param source
     */
    private void addToXmlSource(ArticleSource source) {
        // source document
        Element source_document = doc.createElement("source_document");
        Element source_document_id = doc.createElement("source_document_id");
        source_document_id.setTextContent(source.getId_article_source() + "");
        source_document.appendChild(source_document_id);
        root.appendChild(source_document);

        Element title1 = doc.createElement("title");
        title1.setTextContent(source.getTitle());
        source_document.appendChild(title1);

        Element language = doc.createElement("language");
        language.setTextContent(source.getLang());
        source_document.appendChild(language);

        Element text11 = doc.createElement("text");
        text11.setTextContent(source.getText().toLowerCase());
        source_document.appendChild(text11);

        Element url = doc.createElement("url");
        url.setTextContent(source.getLink());
        source_document.appendChild(url);

        Date now = new Date();
        Element datematched = doc.createElement("datematched");
        datematched.setTextContent(now.toString());
        source_document.appendChild(datematched);
        root.appendChild(source_document);
    }

    /**
     * Mark interval as plagiat
     *
     * @param source_document_id
     * @param source_document_offset_num
     * @param source_document_length_num
     * @param suspicious_document_offset_num
     * @param suspicious_document_length_num
     */
    private void addToXmlInterval(int source_document_id, int source_document_offset_num,
            int source_document_length_num, int suspicious_document_offset_num, int suspicious_document_length_num) {

        Element finding = doc.createElement("feature");
        Element source_document_id_ = doc.createElement("source_document_id");
        source_document_id_.setTextContent("" + source_document_id);
        finding.appendChild(source_document_id_);

        Element source_document_offset = doc.createElement("source_document_offset");
        source_document_offset.setTextContent("" + source_document_offset_num);
        finding.appendChild(source_document_offset);

        Element source_document_length = doc.createElement("source_document_length");
        source_document_length.setTextContent("" + source_document_length_num);
        finding.appendChild(source_document_length);

        Element suspicious_document_offset = doc.createElement("suspicious_document_offset");
        suspicious_document_offset.setTextContent("" + suspicious_document_offset_num);
        finding.appendChild(suspicious_document_offset);

        Element suspicious_document_length = doc.createElement("suspicious_document_length");
        suspicious_document_length.setTextContent("" + suspicious_document_length_num);
        finding.appendChild(suspicious_document_length);

        root.appendChild(finding);
    }

    /**
     * String form Document
     *
     * @param doc
     * @return
     * @throws TransformerException
     */
    public static String getStringFromDocument(Document doc) throws TransformerException {
        DOMSource domSource = new DOMSource(doc);
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.transform(domSource, result);
        return writer.toString();
    }
    
    /**
     * Output xml
     */
    private void saveDocumentToXML() {
        try {
            File file;
            FileOutputStream fop = null;
            String content = getStringFromDocument(doc);

            String filename = FindingTask.getFT().getOutput_XML() + "/" + ticket_id + ".xml";
            file = new File(filename);
            fop = new FileOutputStream(file);

            if (!file.exists()) {   // if file doesnt exists, then create it
                file.createNewFile();
            }

            byte[] contentInBytes = content.getBytes(); // get the content in bytes
            fop.write(contentInBytes);
            fop.flush();
            fop.close();
            log("XML file for suspicious id "+suspicious.getId_article_source()+" created in path: "+filename);

        } catch (TransformerException ex) {
            Logger.getLogger(Comparator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Comparator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Comparator.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * K-grams. It's more faster, then calculate Levenstain distans. I use it
     * not to take the distance where exactly there is nothing similar.
     *
     * @param now
     * @param str1
     * @param str2
     * @return
     */
    private boolean kGrams(int now, int now2) {

        String str = p1.get(now).getText();
        String str2 = p2.get(now2).getText();
        String reg = "[\\,\\-\\'\\\"\\:]";

        boolean result = false;
        if (last != now) { // clean
            str1 = null;
            words = null;
            str1 = str.replaceAll(reg, "").trim().intern();
            words = str.split(" ");
            last = now;
        }

        str2 = str2.replaceAll(reg, "").trim().intern();
        if (str1.length() <= k || str2.length() <= k) { // so small
            return true;
        }

        String[] words2 = null;
        words2 = str2.split(" ");

        int num1 = words.length;
        int num2 = words2.length;

        int num_found = 0;
        for (int i = 0; i < num1; i++) {
            for (int j = 0; j < num2; j++) {
                if (words[i].length() > 3 && words2[j].length() > 3 && words[i].equals(words2[j])) {
                    num_found++;
                }
            }
        }

        if (num_found > 0) {
            result = true;
        }
        return result;
    }

    /**
     * Compare fingerprints
     *
     * @param source
     * @return
     */
    public boolean compareFingerprints(ArticleSource source) {
        Stopwatch sw = new Stopwatch();
        int s_num = suspicious.getFingerprint_array().size();
        int o_num = source.getFingerprint_array().size();
        int num_founded = 0;

        for (int i = 0; i < s_num; i++) {
            for (int j = 0; j < o_num; j++) {
                if (suspicious.getFingerprint_array().get(i).equals(source.getFingerprint_array().get(j))) {
                    log("Ticket id "+suspicious.getId_article_source()+
                            " has a similar fingerprint to the source id "+source.getId_article_source()+" ["+sw.stop()+" sec].");
                    return true;
                }
            }
        }
        log("Ticket id "+suspicious.getId_article_source()+
                " has a dissimilar fingerprint to the source id "+source.getId_article_source()+" ["+sw.stop()+" sec].");
        return false;
    }

    /**
     * Calculate similarity
     *
     * @param i
     * @param j
     * @return
     */
    private double similarity(int i, int j) {
        double l_dist = levDist(p1.get(i).getText(), p2.get(j).getText());
        double similarity = 1 - l_dist / (((double) p1.get(i).getLength() + (double) p2.get(j).getLength()) / 2);
        return similarity;
    }

    /**
     * Add message to log
     *
     * @param message
     */
    public void log(String message) {
        FindingTask.getFT().log(message);
    }

    public int getResult_per() {
        return result_per;
    }
}
