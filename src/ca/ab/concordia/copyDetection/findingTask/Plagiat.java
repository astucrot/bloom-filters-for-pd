/**
 * <p>Title: Plagiat</p>
 * <p>Description: It is copy!</p>
 * <p>Company: Altai State Technical Univesity</p>
 *
 * @author EvgeniyStorozhenko <evgeniy.storozhenko@gmail.com>
 * @date 2013-10-15
 */
package ca.ab.concordia.copyDetection.findingTask;

import java.util.List;
import ca.ab.concordia.copyDetection.dao.model.ArticleSource;

public class Plagiat {

    private int ticket_id;
    private String text;
    private List<ArticleSource> articles_sources;

    public Plagiat(int ticket_id, String text, List<ArticleSource> articles_sources) {
        this.articles_sources = articles_sources;
        this.ticket_id = ticket_id;
        this.text = text;
    }

    public int getTicket_id() {
        return ticket_id;
    }

    public void setTicket_id(int ticket_id) {
        this.ticket_id = ticket_id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<ArticleSource> getArticles_sources() {
        return articles_sources;
    }

    public void setArticles_sources(List<ArticleSource> articles_sources) {
        this.articles_sources = articles_sources;
    }
}
