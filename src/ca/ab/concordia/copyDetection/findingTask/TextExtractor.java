/**
 * <p>Title: TextExtractor</p>
 * <p>Description: Extract text from pdf,txt,doc,docx,rtf,odt</p>
 * <p>Company: Altai State Technical Univesity</p>
 *
 * @author EvgeniyStorozhenko <evgeniy.storozhenko@gmail.com>
 * @date 2014-02-08
 */
package ca.ab.concordia.copyDetection.findingTask;

import ca.ab.concordia.copyDetection.service.FileReaderService;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.apache.tika.detect.DefaultDetector;
import org.apache.tika.detect.Detector;
import org.apache.tika.exception.TikaException;
import org.apache.tika.io.TikaInputStream;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

public class TextExtractor
{

    private static ParseContext context = new ParseContext();
    private static Detector detector = new DefaultDetector();
    private static Parser parser = new AutoDetectParser(detector);
    private static OutputStream outputstream = new ByteArrayOutputStream();
    private static Metadata metadata = new Metadata();

    /**
     * Initialisation
     */
    public static void init()
    {
        context.set(Parser.class, parser);
        context = new ParseContext();
        detector = new DefaultDetector();
        parser = new AutoDetectParser(detector);
        context.set(Parser.class, parser);
        outputstream = new ByteArrayOutputStream();
        metadata = new Metadata();
    }

    /**
     * Convert pdf to string
     * @param path
     * @return
     */
    public static String getTextFromPDF(String path)
    {
        String result = "";
        try
        {
            PDDocument doc = new PDDocument();
            try
            {
                doc = PDDocument.load(path);
            } finally
            {
                if (doc != null)
                {
                    PDFTextStripper stripper = new PDFTextStripper();
                    result = stripper.getText(doc);
                    doc.close();
                }
            }
        } catch (IOException ex)
        {
            Logger.getLogger(FindingTask.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     * Read txt
     * @param path
     * @return
     */
    public static String getTextFromTXT(String path)
    {
        FileReaderService frs = new FileReaderService();
        File file = new File(path);
        return frs.fileReader(file);
    }

    /**
     *
     * @param path
     * @return
     */
    public static String getTextFromOfficeDocs(String path)
    {
        String result = "";

        URL url;
        File file = new File(path);
        try
        {
            if (file.isFile())
            {
                url = file.toURI().toURL();
            } else
            {
                url = new URL(path);
            }
            InputStream input = TikaInputStream.get(url, metadata);
            ContentHandler handler = new BodyContentHandler(outputstream);
            parser.parse(input, handler, metadata, context);
            input.close();
            result = outputstream.toString();
        } catch (MalformedURLException ex)
        {
            Logger.getLogger(TextExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex)
        {
            Logger.getLogger(TextExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex)
        {
            Logger.getLogger(TextExtractor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TikaException ex)
        {
            Logger.getLogger(TextExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;
    }
}
