/**
 * <p>Title: Stopwatch</p>
 * <p>Description: </p>
 * <p>Company: Altai State Technical Univesity</p>
 *
 * @author EvgeniyStorozhenko <evgeniy.storozhenko@gmail.com>
 * @date 2014-01-12
 */

package ca.ab.concordia.copyDetection.findingTask;

public class Stopwatch {
    private final double start;
    
    public Stopwatch() {
        start = (double) System.currentTimeMillis();
    }
    
    public String stop() {
        double end = (double) System.currentTimeMillis();
        return  "" + ((end - start)/1000.0);
    }
}
