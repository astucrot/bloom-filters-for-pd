/**
 * <p>Title: FileReaderService</p>
 * <p>Description: </p>
 * <p>Company: Concordia University College of Alberta</p> 
 * @author    Lu Shi
 * @date       2013-04-07
 */
package ca.ab.concordia.copyDetection.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import ca.ab.concordia.copyDetection.dao.model.TicketVO;

public class FileReaderService {

	public String fileReader(File file) {
		String result = "";
		InputStream in = null;
		try {
			int byteread = 0;
			in = new FileInputStream(file);
			byte[] tempbytes = new byte[in.available()];
			while ((byteread = in.read(tempbytes)) != -1) {
				result += new String(tempbytes).trim();
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e1) {
				}
			}
		}
		return result;
	}

	private String getSql(TicketVO vo) {
		String sql = "insert into tickets(customer_ID, extracted_text) VALUES(30, " + vo.getExtracted_text() + ")";

		return sql;
	}

	public static void main(String[] args) {
		FileReaderService fr = new FileReaderService();
		File file = new File("/home/legion/Dropbox/diplom/MatrixBloomFilter/sources/source1.txt");
		System.out.println(fr.fileReader(file));
	}
}
