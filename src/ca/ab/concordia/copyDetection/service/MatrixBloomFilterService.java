/**
* <p>Title: MatrixBloomFilterService</p>
* <p>Description: </p>
* <p>Company: Concordia University College of Alberta</p> 
* @author    Lu Shi
* @date       2013-04-03
*/
package ca.ab.concordia.copyDetection.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.ab.concordia.copyDetection.dao.impl.ITicketsDao;
import ca.ab.concordia.copyDetection.dao.impl.TicketsDao;
import ca.ab.concordia.copyDetection.dao.model.TicketVO;
import ca.ab.concordia.copyDetection.matrixBloomFilter.BloomFilter;
import ca.ab.concordia.copyDetection.matrixBloomFilter.MatrixBloomFilter;
import ca.ab.concordia.copyDetection.matrixBloomFilter.Paper;
import ca.ab.concordia.copyDetection.util.configuration.ConfigurationManager;

public class MatrixBloomFilterService 
{
	private String falsePositiveProbability = ConfigurationManager.getValueByKey("FalsePositiveProbability");
	private String expectedSize = ConfigurationManager.getValueByKey("ExpectedSize");
	private int chunkSize = Integer.valueOf(ConfigurationManager.getValueByKey("ChunkSize"));
	
	private ITicketsDao dao = new TicketsDao();
	private MatrixBloomFilter mbf = new MatrixBloomFilter();
	
	public String getFalsePositiveProbability() {
		return this.falsePositiveProbability;
	}
	
	public String getExpectedSize() {
		return this.expectedSize;
	}
	
	public int getChunkSize() {
		return this.chunkSize;
	}
	
	public MatrixBloomFilter getMatrixBloomFilter() {
		return this.mbf;
	}
	
	public void readDataFromDBToMarix()
	{
		try {
			List<TicketVO> list = dao.findBySql("select * from tickets where results = ''", TicketVO.class);
			if(mbf.getBloomFilterSize() == 0)
			{
				System.out.println(">");
				TicketVO vo = list.remove(0);
				vo.setExtracted_text(vo.getExtracted_text().replaceAll("\\r\\n"," ").replaceAll("\\pP|\\p{Punct}|\r\n", ""));
				
				BloomFilter bf = new BloomFilter(Float.parseFloat(falsePositiveProbability), Integer.parseInt(expectedSize));
				List<String> elements = getElements(vo.getExtracted_text());
				for(int i = 0; i < elements.size(); i++)
				{
					bf.add(elements.get(i));
				}
				MatrixBloomFilter mbf = new MatrixBloomFilter();
				Paper paper = new Paper();
				paper.setTicketId(vo.getTicket_id());
				mbf.add(bf.getBitSet(), paper);
			}
			for(int i = 0; i < list.size(); i++)
			{
				System.out.println(i);
				TicketVO vo = list.get(i);
				vo.setExtracted_text(vo.getExtracted_text().replaceAll("\\r\\n"," ").replaceAll("\\pP|\\p{Punct}", ""));
				BloomFilter bf = new BloomFilter(Float.parseFloat(falsePositiveProbability), Integer.parseInt(expectedSize));
				List<String> elements = getElements(vo.getExtracted_text());
				for(int j = 0; j < elements.size(); j++)
				{
					bf.add(elements.get(j));
				}
				MatrixBloomFilter mbf = new MatrixBloomFilter();
				Paper paper = new Paper();
				paper.setTicketId(vo.getTicket_id());
				paper.setStatus(vo.getStatus());
				paper.getSimilarPaperMap().putAll(getSimilarRate(bf, elements.size()));
                                paper.setElements_size(elements.size());
				mbf.add(bf.getBitSet(), paper);
				
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Map<Integer,Double> getSimilarRate(BloomFilter bf, int chunkNumber)
	{
		Map<Integer,Double> result = new HashMap<Integer,Double>();
		for(int i = 0; i < mbf.getBloomFilterSize(); i++)
		{
			Paper p = mbf.getPaper(i);
			BitSet bloomFilter = (BitSet) mbf.getBloomFilter(i).clone();			
			bloomFilter.and(bf.getBitSet());		
			double n = 0;
			for(int j = 0; j <bloomFilter.size(); j++)
			{
				if(bloomFilter.get(j))
					n++;
			}
			result.put(p.getTicketId(), n/chunkNumber);
		}
		return result;
	}
	
	public List<String> getElements(String str)
	{
		List<String> list = new ArrayList<String>();
		str = str.trim();
		str = str.replaceAll(" {2,}", " ").intern();
		String[] s = str.split(" ");
		int p = 0;
		while(s.length - p >= chunkSize)
		{
			String r = "";
			for(int i = 0; i < chunkSize; i++)
			{
				r+= new String(s[p+i])+" ";
			}
			r = r.substring(0, r.length()-1).intern();
			p++;
			list.add(r);
		}
		
		return list;
	}
	
	private Paper getPaper(TicketVO vo)
	{
		Paper p = new Paper();
		p.setTicketId(vo.getTicket_id());
		
		return p;
	}

}
