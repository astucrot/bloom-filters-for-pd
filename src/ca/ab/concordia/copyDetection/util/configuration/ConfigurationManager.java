/**
 * <p>
 * Title: ConfigurationManager</p>
 * <p>
 * Description: </p>
 * <p>
 * Company: Concordia University College of Alberta</p>
 *
 * @author Lu Shi
 * @date 2013-03-27
 */
package ca.ab.concordia.copyDetection.util.configuration;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.Scanner;


public class ConfigurationManager {

    final static Charset ENCODING = StandardCharsets.UTF_8;
    private static String FILE_PATH = "configuration/config.properties";

    private static Properties prop;

    private static void loadProperties() {
        try {
            //readLargerTextFile(FILE_PATH);
            InputStream in = new BufferedInputStream(new FileInputStream(FILE_PATH));
            prop.load(in);
            //System.out.println(">"+getValueByKey("Log_path"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* (non-Javadoc)
     * @see ca.ab.concordia.copyDetection.util.configuration.IConfigurationManager#getValueByKey(java.lang.String)
     */
    public static String getValueByKey(String key) {
        if (ConfigurationManager.prop == null) {
            prop = new Properties();
            ConfigurationManager.loadProperties();
        }
        // TODO Auto-generated method stub
        return prop.getProperty(key);
    }

    private static void readLargerTextFile(String aFileName) throws IOException {
        Path path = Paths.get(aFileName);
        try (Scanner scanner = new Scanner(path, ENCODING.name())) {
            while (scanner.hasNextLine()) {
                //process each line in some way
                System.out.println(scanner.nextLine());
            }
        }
    }
}
