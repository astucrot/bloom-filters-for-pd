/**
* <p>Title: MBFServiceTest</p>
* <p>Description: </p>
* <p>Company: Concordia University College of Alberta</p> 
* @author    Lu Shi
* @date       2013-04-03
*/
package ca.ab.concordia.copyDetection.junit;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import ca.ab.concordia.copyDetection.dao.impl.ITicketsDao;
import ca.ab.concordia.copyDetection.dao.impl.TicketsDao;
import ca.ab.concordia.copyDetection.matrixBloomFilter.MatrixBloomFilter;
import ca.ab.concordia.copyDetection.service.FileReaderService;
import ca.ab.concordia.copyDetection.service.MatrixBloomFilterService;
import ca.ab.concordia.copyDetection.util.configuration.ConfigurationManager;

/**
 * @author ShiLu
 *This test case will delete all the records in table tickets.
 */
public class MBFServiceTest {
        
	public void test() throws SQLException {
		ITicketsDao dao = new TicketsDao();
		FileReaderService fr = new FileReaderService();
		MatrixBloomFilterService mbfService = new MatrixBloomFilterService();
		MatrixBloomFilter mbf = new MatrixBloomFilter();
		Map<String,Double> map = new HashMap<String,Double>();
		
		String inputPath = ConfigurationManager.getValueByKey("Input");
		String outputPath = ConfigurationManager.getValueByKey("Output");
		String templatePath = ConfigurationManager.getValueByKey("Template");
		String sourcePath = ConfigurationManager.getValueByKey("Source");
		
		File sourceDir = new File(sourcePath);
		int bloomFilterPosition = 0;
		for(int i = 0; i < sourceDir.listFiles().length; i++)
		{
			map = new HashMap<String,Double>();
			dao.delAll();
			File file = sourceDir.listFiles()[i];
			String content = fr.fileReader(file);
			String sql = "insert into tickets(customer_ID, extracted_text, results, status, extension) values(?,?,?,?,?)";
			dao.saveBySql(sql, 30, content, "","completed","22222");
		
			
			File inputDir = new File(inputPath);
			for(int j = 0; j < inputDir.listFiles().length; j++)
			{
				File inputFile = inputDir.listFiles()[j];
				if(!inputFile.getName().endsWith(sourceDir.listFiles()[i].getName()))
				{
					continue;
				}
				String c = fr.fileReader(inputFile);
				String s = "insert into tickets(customer_ID, extracted_text, results, status, extension) values(?,?,?,?,?)";
				dao.saveBySql(s, 30, c, "",inputFile.getName(),"33333");
			}

			mbfService.readDataFromDBToMarix();
			
			int key = 0;
			
			
                        
			
			
			
                        if (mbf.getPaper(1)!=null && mbf.getPaper(1).getSimilarPaperMap()!=null && mbf.getPaper(1).getSimilarPaperMap().keySet()!=null) {
                                key = Integer.valueOf(mbf.getPaper(1).getSimilarPaperMap().keySet().toString().replace("[","").replace("]",""));
                        }
                        
//                        System.out.println(""+mbf.getPaper(1).getSimilarPaperMap().keySet().size());
//			for(Object k : mbf.getPaper(1).getSimilarPaperMap().keySet())
//			{
//                                System.out.println("+");
//				key = Integer.valueOf(k.toString());
//			}
			
			for(int j = 0; j < mbf.getBloomFilterSize(); j++)
			{
				Map <Integer,Double>m = mbf.getPaper(j).getSimilarPaperMap();
				if(m.keySet().contains(key))
				{
					map.put(mbf.getPaper(j).getStatus(), m.get(key));
				}
			}
			
			File template = new File(templatePath+"/template1.xlsx");
			File targetFile = new File(outputPath+"/"+sourceDir.listFiles()[i].getName().substring(0,sourceDir.listFiles()[i].getName().length()-4)+".xlsx");
			try {
				copyFile(template, targetFile);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			exportToExcel(map, targetFile);
			mbf.delAll();
		}
	}
	
	private void exportToExcel(Map map, File file)
	{
		try {
			InputStream fs = new FileInputStream(file);
			Workbook wb = WorkbookFactory.create(fs);
			Sheet sheet = wb.getSheetAt(0);
			int num = map.keySet().size();
			String[] keys = new String[num];
			int i = 0;
			for(Object key : map.keySet())
			{
				keys[i] = key.toString();
				i++;
			}
			Arrays.sort(keys);
			for(i = 0; i < keys.length; i++)
			{
				sheet.createRow(i);
				Row row = sheet.getRow(i);
				row.createCell(0);
				if (keys[i].length()>0 && keys[i].indexOf("_")!=-1) {
					row.getCell(0).setCellValue(keys[i].substring(0, keys[i].indexOf("_")));
					row.createCell(1);
					row.getCell(1).setCellValue(Double.valueOf(map.get(keys[i]).toString()));
				}

			}
			
			
			
			FileOutputStream fileOut = new FileOutputStream(file);
	        wb.write(fileOut);
	        fileOut.close();
	        fs.close(); 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void copyFile(File sourceFile,File targetFile) throws IOException
	{ 
        FileInputStream input = new FileInputStream(sourceFile);  
        BufferedInputStream inBuff=new BufferedInputStream(input);  
  
        FileOutputStream output = new FileOutputStream(targetFile);  
        BufferedOutputStream outBuff=new BufferedOutputStream(output);  
          
        byte[] b = new byte[1024 * 5];  
        int len;  
        while ((len =inBuff.read(b)) != -1) {  
            outBuff.write(b, 0, len);  
        }  
        outBuff.flush();  
          
        inBuff.close();  
        outBuff.close();  
        output.close();  
        input.close(); 
	}
	
	public static void main(String[] args) throws SQLException
	{
		MBFServiceTest test = new MBFServiceTest();
		test.test();
	}

}
