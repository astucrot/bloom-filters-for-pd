/**
* <p>Title: ConfigurationManagerTester</p>
* <p>Description: </p>
* <p>Company: Concordia University College of Alberta</p> 
* @author    Lu Shi
* @date       2013-03-27
*/
package ca.ab.concordia.copyDetection.junit;


import org.junit.Test;

import ca.ab.concordia.copyDetection.util.configuration.ConfigurationManager;


public class ConfigurationManagerTester {

	@Test
	public void test() 
	{
		ConfigurationManager cm = new ConfigurationManager();
		System.out.println(cm.getValueByKey("DBURL"));
	}

}
