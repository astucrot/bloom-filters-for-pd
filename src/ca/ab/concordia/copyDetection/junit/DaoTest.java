/**
* <p>Title: DaoTest</p>
* <p>Description: </p>
* <p>Company: Concordia University College of Alberta</p> 
* @author    Lu Shi
* @date       2013-04-02
*/
package ca.ab.concordia.copyDetection.junit;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import ca.ab.concordia.copyDetection.dao.impl.TicketsDao;
import ca.ab.concordia.copyDetection.dao.model.TicketVO;

/**
 * @author ShiLu
 *
 */
public class DaoTest {

	@Test
	public void test() {
		TicketsDao dao = new TicketsDao();
		try {
			List<TicketVO> list = dao.findBySql("select * from tickets where results = ''", TicketVO.class);
			for(int i = 0; i < list.size(); i++)
			{
				System.out.println(list.get(i).getTicket_code());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
