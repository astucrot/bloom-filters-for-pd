/**
 * <p>Title: insertSources</p>
 * <p>Description: insert sources from txt to db</p>
 * <p>Company: Altai State Technical Univesity</p>
 *
 * @author EvgeniyStorozhenko <evgeniy.storozhenko@gmail.com>
 * @date 2013-10-31
 */
package ca.ab.concordia.copyDetection.insertSourcesFromTxt;

import java.io.File;
import java.util.Scanner;
import java.io.IOException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import ca.ab.concordia.copyDetection.dao.model.ArticleSource;
import ca.ab.concordia.copyDetection.service.FileReaderService;
import ca.ab.concordia.copyDetection.dao.impl.ArticlesSourcesDao;

public class insertSources {

    private String path_dir;
    private FileReaderService fr;
    private ArticlesSourcesDao daoAS;
    private static insertSources is;

    public static void main(String[] args) {
        // HARDCODE STARTED
        is = new insertSources();

        Scanner sc = new Scanner(System.in);
        System.out.println("Select:");
        System.out.println("1 - Import from file");
        System.out.println("2 - Import from xml");
        System.out.println();
        int select = sc.nextInt();

        if (select == 1) {
            is.getDir();
            is.importTxtIntoDB();
        } else if (select == 2) {
            is.getDir();
            is.parseXML();
        }
    }

    public insertSources() {
        fr = new FileReaderService();
        daoAS = new ArticlesSourcesDao();
    }

    /**
     * input dir
     */
    private void getDir() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Input path: ");
        path_dir = sc.nextLine();
    }

    private void importTxtIntoDB() {
        File inputDir = new File(path_dir);
        for (int j = 0; j < inputDir.listFiles().length; j++) {
            File inputFile = inputDir.listFiles()[j];
            if (inputFile.getName().substring((inputFile.getName().length() - 3), (inputFile.getName().length())).equals("txt")) {
                String c = fr.fileReader(inputFile);
                ArticleSource article_source = new ArticleSource();
                article_source.createFingerprint(c);
                String s = "INSERT INTO articles_sources(title,link,author,text,fingerprint ,lang) values(?,?,?,?,?,?)";
                try {
                    daoAS.saveBySql(s, inputFile.getName(), "", "", c, article_source.getFingerprint(), "en");
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    private void parseXML() {
        File inputDir = new File(path_dir);
        for (int j = 0; j < inputDir.listFiles().length; j++) {
            File inputFile = inputDir.listFiles()[j];
            File inputDirInside = new File(path_dir + inputFile.getName() + "/");

            for (int i = 0; i < inputDirInside.listFiles().length; i++) {
                File inputFileInside = inputDirInside.listFiles()[i];

                if (inputFileInside.getName().substring((inputFileInside.getName().length() - 3), (inputFileInside.getName().length())).equals("xml")) {
                    try {
                        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                        Document doc = dBuilder.parse(inputFileInside);
                        doc.getDocumentElement().normalize();

                        NodeList articles = doc.getDocumentElement().getChildNodes();
                        int num_nodes = articles.getLength();
                        for (int n = 0; n < num_nodes; n++) {
                            String title = "", link = "", text = "", lang = "";
                            NodeList fields = articles.item(n).getChildNodes();
                            int num_fields = fields.getLength();
                            for (int f = 0; f < num_fields; f++) {
                                if (fields.item(f).getAttributes().getNamedItem("name").getNodeValue().equals("lang")) {
                                    lang = fields.item(f).getTextContent();
                                } else if (fields.item(f).getAttributes().getNamedItem("name").getNodeValue().equals("title")) {
                                    title = fields.item(f).getTextContent();
                                } else if (fields.item(f).getAttributes().getNamedItem("name").getNodeValue().equals("text")) {
                                    text = fields.item(f).getTextContent();
                                } else if (fields.item(f).getAttributes().getNamedItem("name").getNodeValue().equals("link")) {
                                    link = fields.item(f).getTextContent();
                                }
                            }
                            insertXMLintoDB(lang, link, title, text);
                        }
                    } catch (ParserConfigurationException ex) {
                        Logger.getLogger(insertSources.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (SAXException ex) {
                        Logger.getLogger(insertSources.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(insertSources.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }

    }

    private void insertXMLintoDB(String lang, String link, String title, String text) {
        ArticleSource article_source = new ArticleSource();
        article_source.createFingerprint(text);
        String s = "INSERT INTO articles_sources(title,link,author,text,fingerprint ,lang) values(?,?,?,?,?,?)";
        try {
            daoAS.saveBySql(s, title, link, "", text, article_source.getFingerprint(), lang);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
