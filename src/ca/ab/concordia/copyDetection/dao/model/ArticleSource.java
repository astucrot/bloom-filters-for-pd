/**
 * <p>Title: Article Source</p>
 * <p>Description: Article from which can copy text</p>
 * <p>Company: Altai State Technical Univesity</p>
 * @author EvgeniyStorozhenko <evgeniy.storozhenko@gmail.com>
 * @date 2013-10-14
 */
package ca.ab.concordia.copyDetection.dao.model;

import java.util.ArrayList;

public class ArticleSource {

    private String link;
    private String text;
    private String title;
    private String author;
    private String lang;
    private String fingerprint;
    private int id_article_source;
    private ArrayList<Integer> fingerprint_array;

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getId_article_source() {
        return id_article_source;
    }

    public void setId_article_source(int id_article_source) {
        this.id_article_source = id_article_source;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getFingerprint() {
        return fingerprint;
    }

    public ArrayList<Integer> getFingerprint_array() {
        return fingerprint_array;
    }

    public void setFingerprint(String fingerprint) {
        this.fingerprint = fingerprint;
    }

    public void setFingerprint_array(ArrayList<Integer> fingerprint_array) {
        this.fingerprint_array = fingerprint_array;
    }

    /**
     * it takes a text and return the text without deliiters
     *
     * @param atext
     * @param subst
     * @return
     */
    private String StripText(String atext, String subst) {
        String[] delimiters = {",", ";", " ", "\\.", "\n", "\t", "|", "\'", "\\*", "-", "'", "\\?", "\\[", "\\]"};
        for (String delimiter : delimiters) {
            atext = atext.replaceAll(delimiter, subst);
        }
        return atext;
    }

    /**
     * Create fingerprint form string
     *
     * @param text
     */
    public void createFingerprint(String text) {
        int gram_size = 30;
        int window_size = 60;
        ArrayList<Integer> hashes = new ArrayList<Integer>();
        String stripped_text = StripText(text, "");
        int text_len = stripped_text.length() - gram_size;

        int offset = 0;
        String curtext = text;
        ArrayList<Integer> values = new ArrayList<Integer>();

        for (int i = 0; i < text_len; i++) {
            offset++;
            int hash = stripped_text.substring(i, (i + gram_size)).hashCode();
            if (hash < 0) {
                hash *= (-1);
            }
            values.add(i, hash);
        }

        // compiling fingerprint
        ArrayList<Integer> fingers = new ArrayList<Integer>();
        ArrayList<Integer> fp = new ArrayList<Integer>();
        int up = text_len - window_size + 1;
        int i = 0;
        int minHashPos = window_size - 1;
        int min_hash = 999999999;
        while (i < up) {
            if (i == 0 || minHashPos == (i - 1)) {
                minHashPos = i + window_size - 1;
                int hash = values.get(minHashPos);
                min_hash = hash;
                for (int j = i + window_size - 1; j >= i; j--) {
                    if (values.get(j) < min_hash) {
                        hash = values.get(j);
                        min_hash = hash;
                        minHashPos = j;
                    }
                }
                i = minHashPos + 1;
                fingers.add(min_hash);
            } else {
                if (values.get((i + window_size - 1)) < min_hash) {
                    minHashPos = i + window_size - 1;
                    int hash = values.get(minHashPos);
                    min_hash = hash;
                    fingers.add(min_hash);
                    i = minHashPos + 1;
                }
            }
        }
        fingerprint_array = fingers;
        fingerprint = "";
        int l = fingers.size();
        if (l > 0) {
            for (i = 0; i < l; i++) {
                fingerprint += fingerprint_array.get(i) + " ";
            }
            if (i == (l - 1)) {
                fingerprint = fingerprint.substring(0, (fingerprint.length() - 2));
            }
        }
    } // end of GetFingerprint

    public void createFingerprintArrayFromString() {
        String[] hashes = fingerprint.split(" ");
        fingerprint = null;
        fingerprint_array = new ArrayList<Integer>();
        for (String str_hash : hashes) {
            if (str_hash.equals("")) {
                str_hash = "0";
            }
            fingerprint_array.add(Integer.parseInt(str_hash));
        }
    }
    
    public void clearText() {
        text = text.replaceAll("\\r\\n", " ").replaceAll("\\pP|\\p{Punct}|\r\n", "").intern();
    }
}
