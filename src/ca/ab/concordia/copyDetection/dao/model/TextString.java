/**
* <p>Title: Num</p>
* <p>Description: </p>
* <p>Company: Altai State Technical Univesity</p> 
* @author EvgeniyStorozhenko <evgeniy.storozhenko@gmail.com>
* @date 2013-11-03
*/
package ca.ab.concordia.copyDetection.dao.model;

public class TextString {
	
	private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
