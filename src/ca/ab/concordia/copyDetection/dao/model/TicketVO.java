/**
* <p>Title: TicketVO</p>
* <p>Description: </p>
* <p>Company: Concordia University College of Alberta</p> 
* @author    Lu Shi
* @date       2013-03-27
*/

package ca.ab.concordia.copyDetection.dao.model;

import java.sql.Timestamp;

public class TicketVO 
{
	private int ticket_id;
	private int stream_id;
	private int customer_id;
	private int result_perc;
	private String ticket_code;
	private String status;
	private String extracted_text;
	private String extension;
	private String result;
	private Timestamp submission_time;
	Timestamp checkout_time;
	int priority;
	public int getTicket_id() {
		return ticket_id;
	}
	public void setTicket_id(int ticket_id) {
		this.ticket_id = ticket_id;
	}
	public int getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(int customer_id) {
		this.customer_id = customer_id;
	}
	public String getTicket_code() {
		return ticket_code;
	}
	public void setTicket_code(String ticket_code) {
		this.ticket_code = ticket_code;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getExtracted_text() {
		return extracted_text;
	}
	public void setExtracted_text(String extracted_text) {
		this.extracted_text = extracted_text;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public Timestamp getSubmission_time() {
		return submission_time;
	}
	public void setSubmission_time(Timestamp submission_time) {
		this.submission_time = submission_time;
	}
	public Timestamp getCheckout_time() {
		return checkout_time;
	}
	public void setCheckout_time(Timestamp checkout_time) {
		this.checkout_time = checkout_time;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}

	public int getResult_perc() {
		return result_perc;
	}

	public void setResult_perc(int result_perc) {
		this.result_perc = result_perc;
	}

	public int getStream_id() {
		return stream_id;
	}

	public void setStream_id(int stream_id) {
		this.stream_id = stream_id;
	}
	
        public void clearText() {
            extracted_text = extracted_text.replaceAll("\\r\\n", " ").replaceAll("\\pP|\\p{Punct}|\r\n", "").intern();
        }
}
