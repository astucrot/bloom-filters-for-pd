/**
* <p>Title: Num</p>
* <p>Description: </p>
* <p>Company: Altai State Technical Univesity</p> 
* @author EvgeniyStorozhenko <evgeniy.storozhenko@gmail.com>
* @date 2013-11-03
*/
package ca.ab.concordia.copyDetection.dao.model;

public class Num {
	
	private int num;

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}
}
