/**
* <p>Title: IBaseDao</p>
* <p>Description: </p>
* <p>Company: Concordia University College of Alberta</p> 
* @author    Lu Shi
* @date       2013-03-27
*/
package ca.ab.concordia.copyDetection.dao.impl;

import java.sql.SQLException;
import java.util.List;


public interface IBaseDao<T> 
{
	public List<T> findBySql(String sql, Class<T> type) throws SQLException;
	public void saveBySql(String sql, Object... params) throws SQLException;
	public int countBySql(String sql) throws SQLException;
        public String getString(String sql) throws SQLException;
	public void delAll() throws SQLException;
	public void delResultIsEmpty()throws SQLException;

}
