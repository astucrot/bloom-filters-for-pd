/**
* <p>Title: ITicketsDao</p>
* <p>Description: </p>
* <p>Company: Concordia University College of Alberta</p> 
* @author    Lu Shi
* @date       2013-04-02
*/
package ca.ab.concordia.copyDetection.dao.impl;

import ca.ab.concordia.copyDetection.dao.model.TicketVO;

/**
 * @author ShiLu
 *
 */
public interface ITicketsDao extends IBaseDao<TicketVO> {

}
