/**
* <p>Title: ArticlesSourcesDao</p>
* <p>Description: </p>
* <p>Company: Altai State Technical Univesity</p> 
* @author EvgeniyStorozhenko <evgeniy.storozhenko@gmail.com>
* @date 2013-10-12
*/
package ca.ab.concordia.copyDetection.dao.impl;

import java.util.List;
import ca.ab.concordia.copyDetection.dao.model.ArticleSource;

public class ArticlesSourcesDao extends BaseDao<ArticleSource> implements IArticlesSourcesDao {

}
