/**
* <p>Title: TicketsDao</p>
* <p>Description: </p>
* <p>Company: Concordia University College of Alberta</p> 
* @author    Lu Shi
* @date       2013-04-02
*/
package ca.ab.concordia.copyDetection.dao.impl;

import java.util.List;

import ca.ab.concordia.copyDetection.dao.model.TicketVO;

/**
 * @author ShiLu
 *
 */
public class TicketsDao extends BaseDao<TicketVO> implements ITicketsDao {

}
