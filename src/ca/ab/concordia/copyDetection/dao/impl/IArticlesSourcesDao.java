/**
* <p>Title: IArticlesSourcesDao</p>
* <p>Description: </p>
* <p>Company: Altai State Technical Univesity</p> 
* @author EvgeniyStorozhenko <evgeniy.storozhenko@gmail.com>
* @date 2013-10-14
*/
package ca.ab.concordia.copyDetection.dao.impl;

import ca.ab.concordia.copyDetection.dao.model.ArticleSource;

public interface IArticlesSourcesDao extends IBaseDao<ArticleSource> {

}
