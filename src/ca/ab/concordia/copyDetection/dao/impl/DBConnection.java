/**
* <p>Title: DBConnection</p>
* <p>Description: </p>
* <p>Company: Concordia University College of Alberta</p> 
* @author    Lu Shi
* @date       2013-03-27
*/
package ca.ab.concordia.copyDetection.dao.impl;

import java.sql.DriverManager;
import java.sql.SQLException;

import java.sql.Connection;

import ca.ab.concordia.copyDetection.util.configuration.ConfigurationManager;

/**
 * @author ShiLu
 *
 */
public class DBConnection 
{
	private static String DBDRIVER = "DBDriver";
	private static String DBURL = "DBURL";
	private static String DBUSER = "DBUser";
	private static String DBPASSWORD = "DBPassword";
	
	private static Connection con;
	
	public static Connection getCon()
	{
		if(DBConnection.con == null)
		{
			try {
				Class.forName("org.gjt.mm.mysql.Driver");
				DBConnection.con = DriverManager.getConnection(ConfigurationManager.getValueByKey(DBConnection.DBURL), ConfigurationManager.getValueByKey(DBConnection.DBUSER), ConfigurationManager.getValueByKey(DBConnection.DBPASSWORD));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return DBConnection.con;
	}

}
