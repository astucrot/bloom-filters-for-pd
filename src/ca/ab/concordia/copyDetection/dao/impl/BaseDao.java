/**
 * <p>Title: BaseDao</p>
 * <p>Description: </p>
 * <p>Company: Concordia University College of Alberta</p> 
 * @author    Lu Shi
 * @date       2013-03-27
 */
package ca.ab.concordia.copyDetection.dao.impl;

import ca.ab.concordia.copyDetection.dao.model.Num;
import ca.ab.concordia.copyDetection.dao.model.TextString;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.activation.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

public class BaseDao<T> implements IBaseDao<T> {

	/* (non-Javadoc)
	 * @see ca.ab.concordia.copyDetection.dao.impl.IBaseDao#findBySql(java.lang.String)
	 */
	@Override
	public List<T> findBySql(String sql, Class<T> type) throws SQLException {
		List<T> list = new ArrayList<T>();
		QueryRunner runner = new QueryRunner();
		BeanListHandler<T> bh = new BeanListHandler<T>(type);
		list = runner.query(DBConnection.getCon(), sql, bh);

		return list;
	}

	public int countBySql(String sql) throws SQLException {
		
		int count = 0;
		List<Num> list = new ArrayList<Num>();
		QueryRunner runner = new QueryRunner();
		BeanListHandler<Num> bh = new BeanListHandler<Num>(Num.class);
		list = runner.query(DBConnection.getCon(), sql, bh);
		if (list!=null && list.size()>0) {
			count = list.get(0).getNum();
		} 

		return count;
	}
        
        /**
         * Get single text field
         * @param sql
         * @return
         * @throws SQLException 
         */
        @Override
        public String getString(String sql) throws SQLException {
		
		String text = "";
		List<TextString> list = new ArrayList<TextString>();
		QueryRunner runner = new QueryRunner();
		BeanListHandler<TextString> bh = new BeanListHandler<TextString>(TextString.class);
		list = runner.query(DBConnection.getCon(), sql, bh);
		if (list!=null && list.size()>0) {
			text = list.get(0).getText();
		} 

		return text;
	}
        

	/* (non-Javadoc)
	 * @see ca.ab.concordia.copyDetection.dao.impl.IBaseDao#daveBySql(java.lang.String, java.lang.Class)
	 */
	@Override
	public void saveBySql(String sql, Object... params) throws SQLException {
		// TODO Auto-generated method stub
		QueryRunner runner = new QueryRunner();
		runner.update(DBConnection.getCon(), sql, params);
	}

	/* (non-Javadoc)
	 * @see ca.ab.concordia.copyDetection.dao.impl.IBaseDao#delAll()
	 */
	@Override
	public void delAll() throws SQLException {
		// TODO Auto-generated method stub
		QueryRunner runner = new QueryRunner();
		runner.update(DBConnection.getCon(), "delete from tickets");
	}

	/* (non-Javadoc)
	 * @see ca.ab.concordia.copyDetection.dao.impl.IBaseDao#delResultIsEmpty()
	 */
	@Override
	public void delResultIsEmpty() throws SQLException {
		// TODO Auto-generated method stub
		QueryRunner runner = new QueryRunner();
		runner.update(DBConnection.getCon(), "delete from tickets where results = ''");
	}
}
