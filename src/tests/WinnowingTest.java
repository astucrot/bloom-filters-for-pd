/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;

import ca.ab.concordia.copyDetection.dao.model.ArticleSource;
import ca.ab.concordia.copyDetection.findingTask.Stopwatch;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author furious
 */
public class WinnowingTest {

    public static String main_doc = "/home/furious/length/1753.txt";
    public static String[] docs = {
        "/home/furious/length/622.txt",
        "/home/furious/length/1091.txt",
        "/home/furious/length/1971.txt",
        "/home/furious/length/2946.txt",
        "/home/furious/length/3943.txt"
    };

    public static void main(String[] args) {
        String main_doc = readFile(WinnowingTest.main_doc);
        ArrayList<Integer> fingerprint_main_doc = createFingerprint(main_doc);
        
        for(String path:WinnowingTest.docs) {
            String doc = readFile(path);
            
            Stopwatch sw = new Stopwatch();
            ArrayList<Integer> fingerprint_doc = createFingerprint(doc);
            System.out.println(sw.stop());
            
        }
    }
    
    public boolean compareFingerprints(ArrayList<Integer> doc1,ArrayList<Integer> doc2) {
        int s_num = doc1.size();
        int o_num = doc2.size();
        int num_founded = 0;

        for (int i = 0; i < s_num; i++) {
            for (int j = 0; j < o_num; j++) {
                if (doc1.get(i).equals(doc2.get(j))) {
                    return true;
                }
            }
        }
        return false;
    }

    public static String readFile(String filename) {
        String content = null;
        File file = new File(filename); //for ex foo.txt
        try {
            FileReader reader = new FileReader(file);
            char[] chars = new char[(int) file.length()];
            reader.read(chars);
            content = new String(chars);
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }
    
    /**
     * Create fingerprint form string
     *
     * @param text
     */
    public static ArrayList<Integer> createFingerprint(String text) {
        int gram_size = 30;
        int window_size = 60;
        ArrayList<Integer> hashes = new ArrayList<Integer>();
        String stripped_text = StripText(text, "");
        int text_len = stripped_text.length() - gram_size;

        int offset = 0;
        String curtext = text;
        ArrayList<Integer> values = new ArrayList<Integer>();

        for (int i = 0; i < text_len; i++) {
            offset++;
            int hash = stripped_text.substring(i, (i + gram_size)).hashCode();
            if (hash < 0) {
                hash *= (-1);
            }
            values.add(i, hash);
        }

        // compiling fingerprint
        ArrayList<Integer> fingers = new ArrayList<Integer>();
        ArrayList<Integer> fp = new ArrayList<Integer>();
        int up = text_len - window_size + 1;
        int i = 0;
        int minHashPos = window_size - 1;
        int min_hash = 999999999;
        while (i < up) {
            if (i == 0 || minHashPos == (i - 1)) {
                minHashPos = i + window_size - 1;
                int hash = values.get(minHashPos);
                min_hash = hash;
                for (int j = i + window_size - 1; j >= i; j--) {
                    if (values.get(j) < min_hash) {
                        hash = values.get(j);
                        min_hash = hash;
                        minHashPos = j;
                    }
                }
                i = minHashPos + 1;
                fingers.add(min_hash);
            } else {
                if (values.get((i + window_size - 1)) < min_hash) {
                    minHashPos = i + window_size - 1;
                    int hash = values.get(minHashPos);
                    min_hash = hash;
                    fingers.add(min_hash);
                    i = minHashPos + 1;
                }
            }
        }
        return fingers;
    } // end of GetFingerprint

    /**
     * it takes a text and return the text without deliiters
     *
     * @param atext
     * @param subst
     * @return
     */
    public static String StripText(String atext, String subst) {
        String[] delimiters = {",", ";", " ", "\\.", "\n", "\t", "|", "\'", "\\*", "-", "'", "\\?", "\\[", "\\]"};
        for (String delimiter : delimiters) {
            atext = atext.replaceAll(delimiter, subst);
        }
        return atext;
    }

}
