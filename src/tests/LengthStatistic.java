/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tests;

import ca.ab.concordia.copyDetection.dao.impl.ArticlesSourcesDao;
import ca.ab.concordia.copyDetection.dao.impl.IArticlesSourcesDao;
import ca.ab.concordia.copyDetection.dao.impl.ITicketsDao;
import ca.ab.concordia.copyDetection.dao.impl.TicketsDao;
import ca.ab.concordia.copyDetection.findingTask.FindingTask;
import ca.ab.concordia.copyDetection.service.MatrixBloomFilterService;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.security.util.BigInt;
import static tests.Winnowing.daoAS;

/**
 *
 * @author storozhenko
 */
public class LengthStatistic
{

    private int num_source_articles = 0;
    private double avgLength = 0;
    
    private ITicketsDao dao;
    private IArticlesSourcesDao daoAS;
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        LengthStatistic length_stat = new LengthStatistic();
        length_stat.init();
        
    }

    private void init()
    {
        initModels();
        countArticles();
        countSumLength();
    }

    private void initModels()
    {
        dao = new TicketsDao();
        daoAS = new ArticlesSourcesDao();
    }
    
    private void countArticles()
    {
        try
        {
            num_source_articles = daoAS.countBySql("SELECT COUNT(*) as num FROM articles_sources");
        } catch (SQLException ex)
        {
            Logger.getLogger(FindingTask.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void countSumLength() {
        int step = 100000;
        try
        {
            int i=0;
            for(i=0;i<num_source_articles;i+=step) {
                
                 double length = daoAS.countBySql("SELECT AVG(part_articles_sources.text_length) as num FROM "
                         + "(SELECT CHAR_LENGTH(text) as text_length FROM articles_sources LIMIT "+i+","+step+") as part_articles_sources");
                 
                 if (avgLength==0) {
                     avgLength = length;
                 } else {
                     avgLength = (avgLength+length)/2;
                 }
                 
                 showResult(i);
                 
            }
           
        } catch (SQLException ex)
        {
            Logger.getLogger(FindingTask.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        
    }
    
    private void showResult(int i) {
        
        System.out.println("\n\nProgress: " +((i / (double) num_source_articles)*100)+"%");
        
        System.out.println("Num articles:        "+num_source_articles);
        System.out.println("Num loaded articles: "+num_source_articles);
        
        System.out.println("Avg langth of articles: "+avgLength);
        
    } 

}
