/**
 * <p>
 * Title: FindingTask</p>
 * <p>
 * Description: Initializes the bloom filter and periodically checks the
 * database for entering a new task. If the task is, then executes it.</p>
 * <p>
 * Company: Altai State Technical Univesity</p>
 *
 * @author EvgeniyStorozhenko <evgeniy.storozhenko@gmail.com>
 * @date 2013-10-12
 */
package tests;

import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.logging.*;
import java.io.IOException;
import java.sql.SQLException;
import java.io.FileNotFoundException;
import ca.ab.concordia.copyDetection.findingTask.*;
import ca.ab.concordia.copyDetection.dao.model.TicketVO;
import ca.ab.concordia.copyDetection.dao.impl.TicketsDao;
import ca.ab.concordia.copyDetection.dao.impl.ITicketsDao;
import ca.ab.concordia.copyDetection.dao.model.ArticleSource;
import ca.ab.concordia.copyDetection.matrixBloomFilter.Paper;
import ca.ab.concordia.copyDetection.dao.impl.ArticlesSourcesDao;
import ca.ab.concordia.copyDetection.dao.impl.IArticlesSourcesDao;
import ca.ab.concordia.copyDetection.service.MatrixBloomFilterService;
import ca.ab.concordia.copyDetection.matrixBloomFilter.MatrixBloomFilter;
import ca.ab.concordia.copyDetection.util.configuration.ConfigurationManager;
import java.text.SimpleDateFormat;

public class Winnowing {

    private Map map;
    private int step;
    private int iterator = 1;
    private int streams_limit;
    public static Logger logger;
    public static int sleep_time;
    private MatrixBloomFilter mbf;
    public static ITicketsDao dao;
    public static String log_path;
    private int max_length_document;
    public static String output_XML;
    public static SimpleDateFormat ft;
    private int articles_sources_limit;
    public static boolean is_debug = false;
    private ArrayList<Stream> streams_list;
    private ArrayList<Paper> paper_tickets;
    public static IArticlesSourcesDao daoAS;
    public static MatrixBloomFilterService mbfService;
    private int[] arr_loading = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    public static void main(String[] args) throws SQLException, InterruptedException, FileNotFoundException, IOException {
        for (String arg : args) {
            if (arg.equals("-debug")) {
                Winnowing.is_debug = true;
            }
        }

        Winnowing ft = new Winnowing();
        ft.init();
    }

    /**
     * Initialisation MBF
     *
     * @throws SQLException
     */
    private void init() throws SQLException, InterruptedException {

        Winnowing.dao = new TicketsDao();
        Winnowing.daoAS = new ArticlesSourcesDao();
        Winnowing.mbfService = new MatrixBloomFilterService();
        Winnowing.logger = Logger.getLogger(Winnowing.class.getName());
        Winnowing.log_path = ConfigurationManager.getValueByKey("Log_path");
        Winnowing.sleep_time = Integer.parseInt(ConfigurationManager.getValueByKey("Sleep"));

        this.map = new HashMap();
        this.paper_tickets = new ArrayList<Paper>();
        this.streams_list = new ArrayList<Stream>();
        this.output_XML = ConfigurationManager.getValueByKey("OutputXML");
        this.step = Integer.parseInt(ConfigurationManager.getValueByKey("Step"));
        this.streams_limit = Integer.parseInt(ConfigurationManager.getValueByKey("Streams_limit"));
        this.articles_sources_limit = Integer.parseInt(ConfigurationManager.getValueByKey("Articles_sources_limit"));
        this.max_length_document = Integer.parseInt(ConfigurationManager.getValueByKey("Max_length_document"));

        Stream.setFound_limit(Integer.parseInt(ConfigurationManager.getValueByKey("Found_limit")));
        Stream.setMin_procent(Double.parseDouble(ConfigurationManager.getValueByKey("Min_procent")));
        readTicketsFromDBToMatrix();
    }

    private void readTicketsFromDBToMatrix() {
        paper_tickets.clear();
        try {
            List<TicketVO> list = Winnowing.dao.findBySql("SELECT * FROM tickets WHERE status='awaiting'", TicketVO.class);
            if (list.size() > 0) {
                ArticleSource source = new ArticleSource();
                source.createFingerprint(list.get(0).getExtracted_text());
                System.out.println(source.getFingerprint());
            }

        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Reserve tickets for this thread
     */
    private void reserveTicketsForThread() {
        String s = "UPDATE tickets SET stream_id='" + iterator + "' WHERE status = 'awaiting' AND stream_id=0";
        try {
            Winnowing.dao.saveBySql(s);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * add/remove stream
     */
    public void newStream() {
        int size = streams_list.size();
        for (int i = 0; i < size; i++) {
            if (!streams_list.get(i).isAlive()) {
                streams_list.remove(i);
                i--;
                size--;
            }
        }

        if (streams_limit > streams_list.size()) {
            streams_list.add(new Stream(iterator));
            streams_list.get(streams_list.size() - 1).setPaper_tickets(new ArrayList<Paper>(paper_tickets));
            streams_list.get(streams_list.size() - 1).setDaemon(true);
            streams_list.get(streams_list.size() - 1).start();
            iterator++;
            Winnowing.log("Num of streams now " + streams_list.size());
        } else {
            Winnowing.log("Streams limit");
        }
        if (iterator > 1000) {
            iterator = 1;
        }
    }

    public static void log(String message) {
        if (Winnowing.is_debug) {
            logger.info(message);
        }

    }
}
